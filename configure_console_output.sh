#!/bin/bash
export ROSCONSOLE_FORMAT='[${time:%H:%M:%S}] [${node}]: ${message}'
export ROSCONSOLE_CONFIG_FILE="`pwd`/rosconsole.config"

